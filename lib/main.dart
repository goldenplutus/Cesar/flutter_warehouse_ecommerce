import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:load/load.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:warehouse/scoped_models/app_model.dart';

import 'app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  debugPaintSizeEnabled = false;

  runApp(ScopedModel<AppModel>(
    model: AppModel(sharedPreferences),
    child: LoadingProvider(
        themeData: LoadingThemeData(
          tapDismiss: false,
        ),
        child: WareHouseApp()),
  ));
}
