class Products {
  String uid;
  String imageURL;
  String productName;
  double price;

  Products({
    this.uid = '',
    this.imageURL = '',
    this.productName = '',
    this.price = 0.0,
  });

  Products.fromJson(Map<dynamic, dynamic> json) {
    uid = json['uid'];
    imageURL = json['imageURL'];
    productName = json['product_name'];
    price = double.parse(json['price'].toString());
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['price'] = this.price.toString();
    data['uid'] = this.uid;
    data['imageURL'] = this.imageURL;
    data['product_name'] = this.productName;

    return data;
  }
}
