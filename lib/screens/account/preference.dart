import 'package:flutter/material.dart';
import 'package:warehouse/screens/view/home.dart';

class Preference extends StatefulWidget {
  Preference({Key key}) : super(key: key);

  @override
  _PreferenceState createState() => _PreferenceState();
}

class _PreferenceState extends State<Preference> {
  bool isFabricSoftenerSwitched = false;
  bool isOxiCleanSwitched = false;
  bool flagCleaning = true;
  bool flagDelivery = false;
  bool flagHypo = false;
  bool flagScent = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF1F0EE),
      appBar: PreferredSize(
        preferredSize: Size(null, 100),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: Color(0xFF315E73), spreadRadius: 5, blurRadius: 2)
          ]),
          width: MediaQuery.of(context).size.width,
          height: 100,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(1.0),
            child: Container(
              color: Colors.transparent,
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 50, 10, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (_) => Home()),
                          );
                        },
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.orange[300],
                          size: 24.0,
                        )),
                    Text(
                      ' My account',
                      style: TextStyle(
                        fontSize: 20,
                        fontFamily: 'Roboto Medium',
                        color: Colors.orange,
                      ),
                    ),
                    Text(
                      '    Preferences',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'Roboto Medium',
                        color: Color(0xFFFFFFFF),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      body: new Container(
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            _buildSideBySideButton(context),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
              child: Text('Cleaning preferences',
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      color: Color(0xFF1D6076))),
            ),
            _buildDetergentChild(context),
            _buildFabricChild(context),
            _buildOxiCleanChild(context),
            _buildStarchChild(context),
          ],
        ),
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Color(0xFFFFFFFF),
            primaryColor: Colors.white,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: TextStyle(color: Color(0xFF868E9C)))),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Color(0xFFD68337),
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.local_offer, size: 26),
              title: Text('Order'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.schedule, size: 26),
              title: Text('Schedule'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle, size: 26),
              title: Text('Account'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.print, size: 26),
              title: Text('Pricing'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.help_outline, size: 26),
              title: Text('Help'),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSideBySideButton(context) => Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        decoration: BoxDecoration(
          color: Colors.white, //remove color to make it transpatent
        ),
        child: Row(
          children: <Widget>[
            SizedBox(width: MediaQuery.of(context).size.width * 0.15),
            Expanded(
                flex: 2,
                child: SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                    child: flagCleaning == true
                        ? RaisedButton(
                            color: Color(0xFF00A3AD),
                            child: Text(
                              "Cleaning",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500),
                            ),
                            onPressed: () {
                              setState(() {
                                flagCleaning = false;
                                flagDelivery = true;
                              });
                            },
                          )
                        : RaisedButton(
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Color(0xFF00A3AD))),
                            color: Colors.white,
                            child: Text(
                              "Cleaning",
                              style: TextStyle(
                                  color: Color(0xFF00A3AD),
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500),
                            ),
                            onPressed: () {
                              setState(() {
                                flagCleaning = true;
                                flagDelivery = false;
                              });
                            },
                          ))),
            Expanded(
              flex: 2,
              child: SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
                child: flagDelivery == false
                    ? RaisedButton(
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Color(0xFF00A3AD))),
                        color: Colors.white,
                        child: Text(
                          "Delivery",
                          style: TextStyle(
                            color: Colors.cyan,
                            fontSize: 18,
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            flagDelivery = true;
                            flagCleaning = false;
                          });
                        },
                      )
                    : RaisedButton(
                        color: Color(0xFF00A3AD),
                        child: Text(
                          "Delivery",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            flagCleaning = true;
                            flagDelivery = false;
                          });
                        },
                      ),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.15,
            ),
          ],
        ),
      );

  Widget _buildDetergentChild(context) => Container(
        height: 60,
        margin: EdgeInsets.fromLTRB(0, 0, 0, 2),
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        decoration: BoxDecoration(
          color: Colors.white, //remove color to make it transpatent
        ),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: Icon(Icons.accessibility),
            ),
            Expanded(
              child: Text(
                "Detergent",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.04,
              width: 120,
              child: flagHypo == false
                  ? RaisedButton(
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Color(0xFF00A3AD))),
                      color: Colors.white,
                      child: Text(
                        "Hypoallergenic",
                        style: TextStyle(
                            color: Colors.cyan,
                            fontSize: 14,
                            fontWeight: FontWeight.w300),
                      ),
                      onPressed: () {
                        setState(() {
                          flagHypo = true;
                          flagScent = false;
                        });
                      },
                      padding: EdgeInsets.all(0),
                    )
                  : RaisedButton(
                      color: Color(0xFF00A3AD),
                      child: Text(
                        "Hypoallergenic",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w300),
                      ),
                      onPressed: () {
                        setState(() {
                          flagHypo = false;
                          flagScent = true;
                        });
                      },
                      padding: EdgeInsets.all(0),
                    ),
            ),
            SizedBox(
                height: MediaQuery.of(context).size.height * 0.04,
                width: 80,
                child: flagScent == true
                    ? RaisedButton(
                        color: Color(0xFF00A3AD),
                        child: Text(
                          "Scented",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w300),
                        ),
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          setState(() {
                            flagScent = false;
                            flagHypo = true;
                          });
                        },
                      )
                    : RaisedButton(
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Color(0xFF00A3AD))),
                        color: Colors.white,
                        child: Text(
                          "Scented",
                          style: TextStyle(
                              color: Color(0xFF00A3AD),
                              fontSize: 14,
                              fontWeight: FontWeight.w300),
                        ),
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          setState(() {
                            flagScent = true;
                            flagHypo = false;
                            
                          });
                        },
                      )),
            SizedBox(
              width: 20,
            ),
          ],
        ),
      );

  Widget _buildFabricChild(context) => Container(
        height: 60,
        margin: EdgeInsets.fromLTRB(0, 0, 0, 2),
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        decoration: BoxDecoration(
          color: Colors.white, //remove color to make it transpatent
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: Icon(Icons.accessibility),
            ),
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Fabric Softener",
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                Text(
                  "Fabric softener is non-hypoallergenic",
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
              ],
            )),
            Container(
              width: 70,
              height: 40,
              margin: EdgeInsets.only(right: 10),
              child: Switch(
                value: isFabricSoftenerSwitched,
                onChanged: (value) {
                  setState(() {
                    isFabricSoftenerSwitched = value;
                  });
                },
                activeTrackColor: Colors.lightBlue,
                activeColor: Colors.white,
              ),
            ),
          ],
        ),
      );

  Widget _buildOxiCleanChild(context) => Container(
        height: 60,
        margin: EdgeInsets.fromLTRB(0, 0, 0, 2),
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        decoration: BoxDecoration(
          color: Colors.white, //remove color to make it transpatent
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: Icon(Icons.accessibility),
            ),
            Expanded(
              child: Text(
                "OxiClean(Whites only",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              width: 70,
              height: 40,
              margin: EdgeInsets.only(right: 10),
              child: Switch(
                value: isOxiCleanSwitched,
                onChanged: (value) {
                  setState(() {
                    isOxiCleanSwitched = value;
                  });
                },
                activeTrackColor: Colors.lightBlue,
                activeColor: Colors.white,
              ),
            ),
          ],
        ),
      );

  Widget _buildStarchChild(context) => Container(
        height: 60,
        margin: EdgeInsets.fromLTRB(0, 0, 0, 2),
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        decoration: BoxDecoration(
          color: Colors.white, //remove color to make it transpatent
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: Icon(Icons.accessibility),
            ),
            Expanded(
              child: Text(
                "Starch (Launder & Press only)",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(right: 25),
                child: Text(
                  'None',
                  style: TextStyle(
                    color: Colors.orange,
                    fontSize: 16,
                  ),
                )),
          ],
        ),
      );
}
