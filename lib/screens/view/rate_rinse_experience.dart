import 'package:flutter/material.dart';

class RateRinseExperience extends StatefulWidget {
  State<StatefulWidget> createState() => new _RateRinseExperienceState();
}

class _RateRinseExperienceState extends State<RateRinseExperience> {
  bool checkingFlight = false;
  bool success = false;

  @override
  Widget build(BuildContext context) {
    return !checkingFlight
        ? Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 30),
                      child: Text(
                        'Hey Jdbdjd, how satisfied are you with your Rinse experience?',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 32,
                          fontWeight: FontWeight.w700,
                        ),
                      )),
                  Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 10, right: 20),
                        child: CircleAvatar(
                          radius: 40,
                          child: IconButton(
                              icon: Icon(
                                Icons.rate_review,
                                color: Colors.orange,
                                size: 16.0,
                              ),
                              onPressed: () {
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(builder: (_) => ShareRinse()),
                                // );
                              }),
                          backgroundColor: Color(0xFF00A3AD),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: CircleAvatar(
                          radius: 40,
                          child: IconButton(
                              icon: Icon(
                                Icons.rate_review,
                                color: Colors.orange,
                                size: 16.0,
                              ),
                              onPressed: () {
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(builder: (_) => ShareRinse()),
                                // );
                              }),
                          backgroundColor: Color(0xFFDE4661),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 100, left: 20),
                    child: Text(
                      'Dismiss',
                      style: TextStyle(color: Colors.orange, fontSize: 20),
                    ),
                  ),
                ]),
          )
        : !success
            ? CircularProgressIndicator()
            : Icon(
                Icons.check,
                color: Colors.green,
              );
  }
}
