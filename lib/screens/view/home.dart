import 'package:flutter/material.dart';
import 'package:warehouse/models/product.dart';
import 'package:warehouse/screens/account/profile.dart';
import 'package:warehouse/screens/account/preference.dart';
import 'package:warehouse/screens/account/rinse_repeat.dart';
import 'package:warehouse/screens/account/rinse_go.dart';
import 'package:warehouse/screens/account/order_history.dart';
import 'package:warehouse/screens/account/share_rinse.dart';
import 'package:warehouse/screens/custom_widget/bottom_sheet.dart';
import 'package:warehouse/screens/view/pricing.dart';
class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 2;

  @override
  void initState() {
    super.initState();
  }

  void onTabTapped(int index) async {
    setState(() {
      _currentIndex = index;
    });

    switch (_currentIndex) {
      case 0:
        {}
        break;

      case 1:
        {}
        break;

      case 2:
        {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Home()));
        }
        break;

      case 3:
        {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Pricing()));
        }
        break;

      case 4:
        {
          // Navigator.pushReplacement(
          //     context, MaterialPageRoute(builder: (context) => MyProfile()));
        }
        break;

      default:
        {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Home()));
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF1F0EE),
      appBar: PreferredSize(
        preferredSize: Size(null, 100),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: Color(0xFF315E73), spreadRadius: 5, blurRadius: 2)
          ]),
          width: MediaQuery.of(context).size.width,
          height: 80,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(1.0),
            child: Container(
              color: Colors.transparent,
              child: Container(
                margin: EdgeInsets.fromLTRB(25, 40, 25, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    new GestureDetector(
                      child: Text(
                        'My Account',
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Roboto Medium',
                          color: Color(0xFFFFFFFF),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      body: new Container(
        child: ListView(
          children: <Widget>[
            Card(
              margin: EdgeInsets.fromLTRB(20, 30, 20, 5),
              child: ListTile(
                leading: const Icon(
                  Icons.perm_identity,
                  color: Colors.orange,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Profile',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
                trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.orange,
                      size: 16.0,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) => Profile()),
                      );
                    }),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: ListTile(
                leading: const Icon(
                  Icons.settings,
                  color: Colors.orange,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Preference',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
                trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.orange,
                      size: 16.0,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) => Preference()),
                      );
                    }),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: ListTile(
                leading: const Icon(
                  Icons.payment,
                  color: Colors.orange,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Payment Method',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
                trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.orange,
                      size: 16.0,
                    ),
                    onPressed: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (_) => ShareRinse()),
                      // );
                    }),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: ListTile(
                leading: const Icon(
                  Icons.autorenew,
                  color: Colors.orange,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Rinse Repeat',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
                trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.orange,
                      size: 16.0,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) => RinseRepeat()),
                      );
                    }),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: ListTile(
                leading: const Icon(
                  Icons.album,
                  color: Colors.orange,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Rinse Go',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
                trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.orange,
                      size: 16.0,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) => RinseGo()),
                      );
                    }),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: ListTile(
                leading: const Icon(
                  Icons.album,
                  color: Colors.orange,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Order history',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
                trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.orange,
                      size: 16.0,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) => OrderHistory()),
                      );
                    }),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: ListTile(
                leading: const Icon(
                  Icons.album,
                  color: Colors.orange,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Share Rinse',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
                trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.orange,
                      size: 16.0,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) => ShareRinse()),
                      );
                    }),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: ListTile(
                leading: const Icon(
                  Icons.album,
                  color: Colors.orange,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Love Rinse',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
                trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.orange,
                      size: 16.0,
                    ),
                    onPressed: () {
                      showModalBottomSheet(
                          isScrollControlled: true,
                          context: context,
                          builder: (context) {
                            return FractionallySizedBox(
                              heightFactor: 0.6,
                              child: Container(
                                color: Color(0xFF1D6076),
                                height: 500,
                                child: Container(
                                  child: BottomSheetWidget(),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).canvasColor,
                                  ),
                                ),
                              ),
                            );
                          });
                    }),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: ListTile(
                leading: const Icon(
                  Icons.face,
                  color: Colors.orange,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Facebook Connect',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
                trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.orange,
                      size: 16.0,
                    ),
                    onPressed: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (_) => ShareRinse()),
                      // );
                    }),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Text('\$20 Credit',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 24,
                      color: Colors.cyan[900])),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
              child: RichText(
                text: new TextSpan(
                  text: 'Log out',
                  // recognizer: new TapGestureRecognizer()
                  //   ..onTap = () => Navigator.of(context).push(
                  //           MaterialPageRoute<Null>(
                  //               builder: (BuildContext context) {
                  //         return OthersProfile(
                  //           uid: activityList[index][7],
                  //         );
                  //       })),

                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.orange,
                      fontWeight: FontWeight.w400),
                  children: <TextSpan>[
                    TextSpan(
                        text: ' ' + 'of my account',
                        style: TextStyle(
                            fontFamily: 'Roboto Black',
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: Colors.black)),
                  ],
                ),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Color(0xFFFFFFFF),
            primaryColor: Colors.white,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: TextStyle(color: Color(0xFF868E9C)))),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Color(0xFFD68337),
          currentIndex: _currentIndex,
          onTap: onTabTapped,
          
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.local_offer, size: 26),
              title: Text('Order'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.schedule, size: 26),
              title: Text('Schedule'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle, size: 26),
              title: Text('Account'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.print, size: 26),
              title: Text('Pricing'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.help_outline, size: 26),
              title: Text('Help'),
            ),
          ],
        ),
      ),
   
    );
  }
}
